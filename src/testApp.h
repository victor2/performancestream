#pragma once

#include "ofMain.h"
#include "ofxMidi.h"
#include "ofxOpenCv.h"

#include "ofxIniSettings.h"

#include "imgPlayer.h"
#include "Pop3Client.h"

#define DOWNLOAD_PERIOD 10000
#define EMAIL_TIMEOUT 20000

class MidiControl {
    int mn, mx;
    int *v;
    float *fv;
    int ch;
    string nm;
    bool init(ofxIniSettings &ini, string id);
public:
    void init(ofxIniSettings &ini, string id, int *var);
    void init(ofxIniSettings &ini, string id, float *var);
    bool handle(ofxMidiMessage& e);
};

class testApp;
class ImageProvider;
class CameraWriter : public ofThread
{
        list<ofVideoGrabber*>::iterator grIter; // the currently used camera
        int icam;
        int cnt; // image counter
        int wd, ht;
        int camDelay; // after-init delay
        int *period, *outline;
        string path;
        char prefix[50];
        list<ofVideoGrabber*> grabbers;

        // arduino control
        ofSerial* serial;
        ImageProvider* m_ip;

    public:
        CameraWriter();
        bool init(string ids, ImageProvider *ip);
        bool initGrabber(int id);
        void setTiming(int *pe, int *o) {
            period = pe;
            outline = o;
        }
        void setPath(string &p);
        void setDimensions(int w, int h, int delay) {
            wd = w; ht = h; camDelay = delay;
        }
        void setSerial(ofSerial *s) { serial = s; }
        void capture();
        void threadedFunction();
};

class Pop3Writer : public ofThread
{
    ImageProvider *m_ip;
    Pop3Client m_pop3;
    public:
        Pop3Writer() {}
        void setApp(ImageProvider *ip, testApp *a, ofxIniSettings &ini, string path, int *o);

        void fetch();
        void threadedFunction();
};

class ImageProvider {
        enum State { Init, Email, Camera, File } state;
        long t0;
        int period;
        int *stock;
        string      usedDir;
        string stockDir;
        std::list<string> files;
        CameraWriter &cameraWriter;
        Pop3Writer &pop3Writer;
        bool email;
    public:
        ImageProvider(CameraWriter &cw, Pop3Writer &pw) : cameraWriter(cw), pop3Writer(pw)
        {
            t0 = 0;
            state = Init;
        }
        void init(bool e, int p, int *s, string dir, string udir);
        void addImageFile(string fn);
        void capture();
        string nextImageFile();
        void revertTimer() { t0 -= EMAIL_TIMEOUT; state = File; }
};


class testApp : public ofBaseApp, public ofxMidiListener {
        ofxIniSettings ini;

		int 		nImages;
		ofImage     deleted; // to be shown instead of deleted image
        ofDirectory   DIR;

        ofImage     currentImage;
        int         currentFrame; // reset when a new image gets loaded
        // protected area of the current image
        ofRectangle active;
        //
        ofRectangle getActiveArea(int width, int height);
        void reduceRect(ofRectangle &rct);
        void shakeRect(ofRectangle &rct);
        bool getNewImages();
        void fileMove(string file, string destDir);

        float       scrWd;
        float       scrHt;
        ofTexture   screen;

        int         period; // download period
        string      updated; // the file name of a recently downloaded image
        int         imgIdx;
        // configuration flags
        bool        email; // images can be loaded from email

        // local source parameters
        string      resDir;
        string      usedDir;
        string      stockDir;
        bool        filemove;

        CameraWriter capturer;
        Pop3Writer  pop3writer;
        ofxCvContourFinder cf;

        // additional info can be shown
        bool        info;
        string      infoFile; // the name of the info image

        // text info by pressing F1
        bool        f1;

        imgPlayer   imp;
        ofSoundPlayer player;
        string      sndFileA, sndFileB, sndFile; // names of sound files - A, B, result
        string      sndImgFile; // image file to make sound of
        // frequency ranges
        float       lowFreq;
        float       hiFreq;

        int         alpha; // set 0 to disable
        int         shake;

        int         stock; // probability of showing stock inages, %

        int         r, g, b; // colour components of monochrome images
        int         movX, movY; // permanent addition to random shake
        int         outline; // for bw images
        int         spread; // visual effect

        ofxMidiIn   midiIn;
        MidiControl alphaCtr, shakeCtr;
        MidiControl lowCtr, highCtr;
        MidiControl periodCtr, stockCtr;
        MidiControl rCtr, gCtr, bCtr;
        MidiControl movXCtr, movYCtr;
        MidiControl spreadCtr, outlineCtr;

        // screen dumping
        int         dump;
        long        dumpedAt;

        // arduino control
        ofSerial    serial;

        // time of last image transition
        long        t0;

        // in conversion mode
        const char *imagefile;

	public:
        // to be acccessed from pop client
        ImageProvider provider;

        testApp(const char *imgf) : provider(capturer, pop3writer) { t0 = 0; imagefile = imgf; }

		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

        void newMidiMessage(ofxMidiMessage& eventArgs);
        void newSound(const char *snd, const char *img) { sndFile = snd; infoFile = img; }
        // notification of new iamges
        void newimages(string a, string b);
};
