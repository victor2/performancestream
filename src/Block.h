// Block.h: interface for the Block class.
// Block is a container for a signal at limited time interval.
// Each sample is represented as 'double' value.
//////////////////////////////////////////////////////////////////////

#if !defined(_BLOCK_H_)
#define _BLOCK_H_

#include <assert.h>
#include <math.h>
#include <memory.h>

#if !defined(LPCSTR)
#define LPCSTR const char *
#endif

/////////////////////////////////////////////////////////
// RmaxType - is used to store result of search min & max values
//
struct RmaxType {
	double fMax;
	double fMin;
	int iMax;
	int iMin;
};

/////////////////////////////////////////////////////////
// DomainType
//	Data in Block is a function of time,
//	or of frequency (complex data),
//	or spectrum density - right half of data is == 0;
//		it's result of Sxx().
enum DomainType {
		dmTime, dmComp, dmDensity
	};

/////////////////////////////////////////////////////////
// ArgumType
//	if argument type is atLinear, argument value
//	should be calculated as Left() + i * Calx(),
//	else	as Left() * Calx() ^ i
enum ArgumType {
		atLinear, atPow
	};

typedef bool (*progressFunc)(void*par);

/////////////////////////////////////////////////////////
// Block
//
class Block
{
public:
	Block();
	Block(int iSize);
	Block(const Block & src);
	virtual ~Block();
	void Init(int iSize);
	// �������������
	Block & Fill(double fVal=0, int iKd=0, int iKf=-1);
    // �����������
    Block & CopyData( const Block & bSrc, int iKd=0, int iKf=-1, int iKn=0 );
	// ����������
	Block & Add(const Block & bOper);
	Block & Sub(const Block & bOper);
	Block & Mul(const Block & bOper);
	Block & Dib(const Block & bOper);
	Block & Add(const double & fOper);
	Block & Mul(const double & fOper);
	Block & Sqrt();

	//  ���������� ����������� ��������� � ��������� �� 10.
	Block & LogX10();	// to dB

	//  ��������, �������� � LogX10.
	Block & PowX10();	// from dB

	// ������
	double & operator [] (int iIndx);

	bool CheckRange(bool bDoCheck);

	// ���������� ������ �����
	int GetSize() const;

	// ���������� ������ �� ��� ������ � �����
	DomainType & GetDomain();
	ArgumType & GetArgumentType();

	void FindMax(RmaxType & rResult, int iKd=0, int iKf=-1) const;

	// ���������� ������ �� ��� ����� ��������� ������ � �����
	double & Calx(void);

	// ���������� ������ �� �������� �������� �������
	double & Left(void);
	// ������

	//  �������� ������������ ������ � �����.
	Block & Linc();

	//  Hermith smoothing.
    // If closed == true, the first and last result valus
    // must be the same or close.
	Block & Hermith( int closed );

	//  �������� ���������� ������������ �� ����� - ����������
	//  �������� � ��� ���������
	Block & Center();

	//  ���������� ����������� ��������, ��������� � ����� this
	//  ����� ��������� kd � kf (������������).
	//  ��������� ������������� � ����� dest. ����� ������ �����������
	//  ������� ������������ ������� - ��������, dest.fill(); .
	//  ��� ��������� ���� �������� � ���������� �������� (����������������
	//  �������� ������� ����� dest) ������� ������������
	//  dest.calx() �  dest.left().
	Block & Hist(Block & dest, int kd=0, int kf=-1);

	//  ����������� �������� ����� ��������� max, min.
	Block & Limt(double max,double min);

	//  ���������� �������� �������� �����.
	Block & Fabs();

	//  ���������� ����� �������� � ����� ���������.
	//  kd, kf - ��������� � �������� ������� ���� ������������.
	//  ����� : �����.
	double Area( int kd=0, int kf=-1 ) const;

	//  ���������� �������� ��������������� � ����� ����� blo.
	//  kd, kf - ��������� � �������� ������� ���� ������������.
	//  ����� : ������� ��������������.
	double Average( int kd=0, int kf=-1 ) const;

    //++++++++++++++++++++++++++++++++++++++++++++++++
    // � ����� ������� ���������� �� ������� ����� nt, ���� ��� ������
	Block &Envelope( Block &nt );

	//  ���������� ������������������� ���������� � ����� �����.
	//  kd, kf - ��������� � �������� ������� ���� ������������.
	//  ����� : �����������������e ���������e.
	double RMS( int kd=0, int kf=-1 ) const;

	// �������������� �������� �����
	// p�������� ��������� �� ����� �������� ������.
	Block & Integral();

	// ��������� ����������������� ������ �����
	// p�������� ��������� �� ����� �������� ������.
	Block & Derivate();

	//  ����������� ������� ����� (-1/4, +1/2, -1/4 ).
	Block & Hamming();

	// �������������� �����
	Block & FFT();

	// �������� �������������� �����
	Block & TFF();

	//  ������� ������ ������������ ������ ����� ns.
	//  ����: ����������� ������ �������.
	//  ��������� ��������� � ������ �������� �����.
	Block & Sxx();

	//  �������� ������ ������ ����� � ������ bOp.
	//  ����: ����������� ������� ���������.
	Block & Sxy(Block & bOp);

	//  ������� ������ ������������ ������ ����� ns.
	//  ����: ������ �� ��������� �������������.
	//  ��������� ��������� � ������ �������� �����.
	//  �������� ������������� hamm() � logX10().
	Block & SpDens(bool bDoHamm=false, bool bDoLogX10=false);

	//  ������� ����������� ������ � ������������� �����.
	//  ������� ������ ������������� ���� - � ����� this.
	//  ����� : module - ���� �������, phase - ���� ���.
	Block & ModPhase(Block & bModule, Block & bPhase);

	// ������� ����������� ������ �� ������������� �����.
	//  ���� : module - ���� �������, phase - ���� ���.
	//  ��������� ( ������ ������������� ���� ) ��������� � ���� this.
	Block & DeModPhase(Block & bModule, Block & bPhase);

    // ������� � �����, ������� ��������� � ����� flt
	Block & Convolution(Block & flt, progressFunc pfun = 0, void * parg = 0, int steps = 100);

	/////////////////////////////////////////////////////////
	// �������� ��������

	// ����� ��������� �� ����� ����, � ����� - �������,
	// � ������ ������� ������ � ������� �������� �������
	bool Load( LPCSTR szFileName, int iFileOffset=0, int iCntsNumber=-1 );

	// ��� ������ �� ������� ����� ������ �����, ���������
	// � ���������, ������������� ��������������.
	bool Save( LPCSTR szFileName, int iFileOffset=0, int iCntsNumber=-1 );

	//
	static int log2(int iVal);
//	void Visu(TImage * Image, double min=0, double max=0, int divX=0, int divY=0 );
//	void VisuRng(TImage * Image, int kd = 0, int kf = -1, double min=0, double max=0, int divX=0, int divY=0 );
//    // Style: TRect is not a reference - it might be modified inside!
//	void VisuRng(TCanvas * canv, TRect rect, int kd = 0, int kf = -1, double min=0, double max=0, int divX=0, int divY=0 );

    const char * GetUnitX() { return unitX; }
    const char * GetUnitY() { return unitY; }
    void SetUnitX(const char *un) { strncpy(unitX,un,sizeof unitX); }
    void SetUnitY(const char *un) { strncpy(unitY,un,sizeof unitY);  }
private:
	int m_iSize;		// data size - number of samples
	double * m_fData;	// pointer to signal
	double m_fLeft;	// start time, if block in time domain.
	double m_fCalx;	// sampling period
	double m_fPow;	// used with m_eArgType==atPow
	DomainType m_eDomain;
	ArgumType m_eArgType;
	bool m_bDoCheckRange;
	static double m_fZero;
    char unitX[10];
    char unitY[10];
protected:
	void ExpandKdKf(int & iKd, int & iKf) const;
};

/////////////////////////////////////////////////////////
// inline - ������� ������ Block
//
inline double & Block::operator [] (int iIndx)
{
	if(iIndx<0 || iIndx>=m_iSize)
	{
		assert(!m_bDoCheckRange);
		return m_fZero=0;
	}
		else return m_fData[iIndx];
}

inline int Block::GetSize(void) const
{
	return m_iSize;
}

inline DomainType & Block::GetDomain()
{
	return m_eDomain;
}

inline ArgumType & Block::GetArgumentType()
{
	return m_eArgType;
}

inline double & Block::Calx(void)
{
	return m_fCalx;
}

inline double & Block::Left(void)
{
	return m_fLeft;
}

/////////////////////////////////////////////////////////
// digital filter
//
const int Max_Filter_Power = 40;
class CFilter {

    class Zveno {
        double
	        p1,p2,a1,a2,b;
    public:
	    Zveno(void) { init(); }
    	void init() { p1=p2=0; }
    	void coeff(double A1, double A2 ,double B) { a1=A1; a2=A2; b=B; }
	    double filt(double f)
    	{
	    	f=f*b-a1*p1-a2*p2;
		    p2=p1;
    		p1=f;
	    	return f;
    	}
    };

    int Half_Filter_Power;
    double
        A1[Max_Filter_Power],
	    A2[Max_Filter_Power],
    	BZERO;
    Zveno m_zv[Max_Filter_Power / 2];
public:
    CFilter() { Half_Filter_Power = 0; }
    virtual void bandPass( double FC, double BW, int power );
    double filtrate( double  val );
    virtual void init();
};

class CBlockFilter: public CFilter {

public:
    virtual void filtrate( Block & blo,
    	progressFunc pfun =0, void * parg =0, int steps = 100 );
};

/////////////////////////////////////////////////////////
// ��������� ����� ��� ���������� ������
//
struct CBlockFileHeader {
	char ID[4];		// ������������� - ���� ������ ���������� � BLOW
	int iCntsNumber;	// ����� ��������
	int iDataOffset;	// �������� ������� ������� ������ �� ������ �����
	double fStart;	// ��������� �������� - �����, ������� � �.�. (������ Block::Left() )
	double fStep;	// ��� ����� ��������� - ������ Block::Calx()
	DomainType eDomain;
	ArgumType eArgType;
};

/////////////////////////////////////////////////////////
// ����� ��� ��������� ���������� �� ������ ��� ���������� ������
//
class CBlockFile {
	CBlockFileHeader m_cFileHeader;
	bool m_bIsValid;
public:
	CBlockFile(LPCSTR szFileName);
	bool IsValid();
	int GetSize();
	int GetDataOffset();
	double GetStart();
	double GetStep();
	ArgumType GetArgumentType();
	DomainType GetDomainType();
};
/////////////////////////////////////////////////////////
// inline - ������� ������ CBlockFile
//
inline bool CBlockFile::IsValid()
{
	return m_bIsValid;
}

inline int CBlockFile::GetSize()
{
	return m_cFileHeader.iCntsNumber;
}

inline int CBlockFile::GetDataOffset()
{
	return m_cFileHeader.iDataOffset;
}

inline double CBlockFile::GetStart()
{
	return m_cFileHeader.fStart;
}

inline double CBlockFile::GetStep()
{
	return m_cFileHeader.fStep;
}

inline ArgumType CBlockFile::GetArgumentType()
{
	return m_cFileHeader.eArgType;
}

inline DomainType CBlockFile::GetDomainType()
{
	return m_cFileHeader.eDomain;
}

// ������ �� ����� ������, ����� ������������ ������,
// ������� �������� �������/���������
class CTablesKiller {
public:
	~CTablesKiller() { FreeMemory(); }
	void FreeMemory();
};
extern CTablesKiller TableKiller;

#endif // !defined(_BLOCK_H_)
