#ifndef POP3CLIENT_H
#define POP3CLIENT_H

#include <list>
#include "Poco/Net/POP3ClientSession.h"
#include "Poco/Net/MailMessage.h"
using namespace std;

class ofxIniSettings;
class ImageProvider;
class Pop3Client
{
    public:
        Pop3Client();
        virtual ~Pop3Client();
        void init(ofxIniSettings &ini);
        void init(string host, int port, string user, string pass, string subject) {
            m_Host = host;
            m_Port = port;
            m_User = user;
            m_Pass = pass;
            m_Subject = subject;
        }

        void setPath(string &p, int *o, ImageProvider *a) {
            m_Path = p;
            m_pOutline = o;
            m_Ip = a;
        }

        int gotImages(); // returns the number
    protected:
    private:
        ImageProvider *m_Ip;
        string m_Host;
        string m_User;
        string m_Pass;
        string m_Subject; // only letters with this word in subject are processed
        int m_Port;

        int *m_pOutline;
        int m_Cnt; // image counter
        char m_Prefix[100]; // a part of a filename
        string m_Path; // where to store image files
};

#endif // POP3CLIENT_H
