#ifndef IMGPLAYER_H
#define IMGPLAYER_H

#include "ofSoundPlayer.h"
#include "ofImage.h"
#include "ofThread.h"
#include "Block.h"

struct waveformat {
WORD
	wFormatTag,	// must be 1
	nChannels;	// stereo/mono
DWORD
	nSamplesPerSec,
	nAvgBytesPerSec;
WORD
	nBlockAlign,
	wBitsPerSample;
};

struct wave_head {
	char RIFF[4];
	DWORD len1;
	char WAVEfmt[8];
	DWORD len2;
    waveformat fmt;
	char data[4];
	DWORD len3;
};

class testApp;
// To be precise, it's not a player. It's a convertor.
class imgPlayer : public ofThread
{
    public:
        const int SAMPLING;
        const int BSZ; // block size

        imgPlayer();
        virtual ~imgPlayer();
        void convert(ofImage &img, const char *sndFile, float low, float high);
        static void prepareBW(ofImage &src, ofImage &dest, int outln);

        void startConversion(const char *img, testApp *a, float low, float high);
        void threadedFunction();

    protected:
    private:
        static ofxCvContourFinder cf;
        testApp *app;
        float  lowFreq;
        float  hiFreq;
        string fname;

        Block bCosWin; // cos^2
        // loads the line of pixels with given x from pic to blo so that
        // blo[0] = pic.pixels[x][H]
        // ...
        // blo[i] = pic.pixels[x][H-i]
        // where H = pic.height - 1 // the index of the lowest row of pixels
        void LoadLine(ofImage &pic, Block &blo, int x);
        // uses frequency range specified in lowFreq and hiFreq
        void LoadLineRange(ofImage &pic, Block &blo, Block &tmp, int x, int kd, int kf);

        void InvFourieProc(ofImage &pic, short *wave, int kd, int kf);

        void writeWav(const char * filename, int nch, int cnt, short *wave);

        // allocates a new array and returns its size in cnt
        short * readWav(const char * filename, int &cnt);
};

#endif // IMGPLAYER_H
