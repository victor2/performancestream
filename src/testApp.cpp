#include <ofLog.h>
#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup() {
    f1 = false;
    ofSetWorkingDirectoryToDefault();
    ini.load("config.ini");
    string log = ini.get("log", string("warning"));
    if (log == "notice") {
        ofLogLevel(OF_LOG_NOTICE);
    }

    ofLogToFile("pers.log", false);
    //ofLogLevel(OF_LOG_VERBOSE);
    ofLogNotice("Starting");

    // https://github.com/openframeworks/openFrameworks/issues/771
    ofDisableArbTex();

    period = ini.get("period", DOWNLOAD_PERIOD);
    email = ini.get("email",string("false")) == "true";
    int w = ini.get("width", 640);
    int h = ini.get("height", 480);
    int delay = ini.get("delay", 500);

    if (!imagefile) {
        // don't initialize the camera because we're going to exit after image conversion
        capturer.setDimensions(w, h, delay);
        capturer.setTiming(&period, &outline);
        string camids = ini.get("cameraid", string("0"));
        if (!capturer.init(camids, &provider))
        {
            cout << "FATAL : failed to init camera(s) " << camids << ". Closing application." << endl;
            ::exit(1);
        }
    }

    resDir = ini.get("resdir", string("images\\res"));
    usedDir = ini.get("useddir", string("images\\used"));
    stockDir = ini.get("stockdir", string("images\\stock"));

    capturer.setPath(resDir);
    pop3writer.setApp(&provider, this, ini, resDir, &outline);
    provider.init(email, period, &stock, stockDir, usedDir);

    currentImage.allocate(1, 1, OF_IMAGE_COLOR_ALPHA);

    nImages = DIR.listDir("images");
    //you can now iterate through the files as you like
    for(int i = 0; i < nImages; i++){
        string fn = DIR.getPath(i);
        ofDirectory dir(fn);
        if (dir.isDirectory()) continue;
        if (fn == "images\\deleted.jpg") continue;
        bool ok = currentImage.loadImage(fn);
        ofLog() << fn << " " << ok << endl;
        if (ok) break;
    }

    deleted.loadImage("images\\deleted.jpg");

    int wd = ofGetScreenWidth();
    int ht = ofGetScreenHeight();
    screen.allocate(wd, ht, GL_RGB);
    ofSetWindowShape(wd/2, ht/2);
    ofSetWindowPosition(wd/4, ht/4);

	//for smooth animation, set vertical sync if we can
	ofSetVerticalSync(true);
	// also, frame rate:
	ofSetFrameRate(20);
    currentFrame = 0;
    updated = "init";

    stock = ini.get("stock", 0);

    imgIdx = 0;
    // transparency/fade transition
    alpha = ini.get("alpha", 0);
    shake = ini.get("shake", 0);

    // sound generator
    lowFreq = ini.get("lowB", 400);
    hiFreq = ini.get("highB", 14000);

    alphaCtr.init(ini, "alpha", &alpha);
    shakeCtr.init(ini, "shake", &shake);
    lowCtr.init(ini, "low", &lowFreq);
    highCtr.init(ini, "high", &hiFreq);
    periodCtr.init(ini, "period", &period);
    stockCtr.init(ini, "stock", &stock);


    player.setLoop(true);
    player.setSpeed(1.0);
    player.setVolume(1.0);
    player.setMultiPlay(false);

    // colour control
    r = ini.get("red", 0);
    g = ini.get("green", 0);
    b = ini.get("blue", 0);
    rCtr.init(ini, "red", &r);
    gCtr.init(ini, "green", &g);
    bCtr.init(ini, "blue", &b);

    movX = ini.get("movX", 0);
    movY = ini.get("movY", 0);
    movXCtr.init(ini, "movX", &movX);
    movYCtr.init(ini, "movY", &movY);

    spread = ini.get("spread", 3);
    outline = ini.get("outline", 1);
    spreadCtr.init(ini, "spread", &spread);
    outlineCtr.init(ini, "outline", &outline);

    if (imagefile) {
        cout << "converting " << imagefile << " " << lowFreq << "..." << hiFreq << endl;
        ofImage img;
        ofDisableDataPath();
        if (img.loadImage(imagefile)) {
            imgPlayer::prepareBW(img, img, outline);
            string fn = imagefile;
            fn = fn.substr(0, fn.length() - 4) + "-b.jpg";
            img.saveImage(fn);
            cout << "saved " << fn << endl;

            imgPlayer p;
            p.convert(img, "snd.wav", lowFreq, hiFreq);

            ofSoundPlayer player;
            player.setLoop(false);
            player.setSpeed(1.0);
            player.setVolume(1.0);
            player.loadSound("snd.wav");
            player.play();
            do {
                ofSleepMillis(500);
            }
            while (player.getIsPlaying());
            ::exit(1);
        }
    }


    // screen dumping
    dump = ini.get("dump", 0);
    dumpedAt = ofGetElapsedTimeMillis() + 10000; // start dumping a bit later

    // This outputs the various ports
    // and their respective IDs.
    midiIn.listPorts();
    if (midiIn.getNumPorts())
    {
        string midi = ini.get(string("midi"), string("nano"));
        int np = 0;
        for (int i=0; i<midiIn.getNumPorts(); ++i) {
            string nm = midiIn.getPortName(i);
            int p = nm.find(midi);
            if (p >= 0) {
                ofLogNotice() << "Using " << nm << endl;
                np = i;
            } else {
                ofLogNotice() << "Not using " << nm << endl;
            }
        }

        // Now open a port to whatever
        // port ID your MIDI controller is on.
        midiIn.openPort(np);
        // Add this app as a listener.
        midiIn.addListener(this);
    }

   	serial.enumerateDevices();
	int baud = ini.get("rate", 57600);
    if (serial.setup(ini.get("com", string("COM1")).c_str(), baud))
    {
        capturer.setSerial(&serial);
    }

    srand(ofGetElapsedTimeMillis());
}

//--------------------------------------------------------------
void testApp::update(){

    long now = ofGetElapsedTimeMillis();
    // screen dumping
    if (dump && (now > dumpedAt + dump) && screen.bAllocated())
    {
        ofImage tmp;
        tmp.grabScreen(0, 0, scrWd, scrHt);
        ofSaveImage(tmp, "images\\dump.jpg", OF_IMAGE_QUALITY_HIGH);
        dumpedAt = now;
        cout << "dump.";
    }

    if (getNewImages())
    {
        t0 = now;
    }

    ofImage img;
    bool initial = active.width == 0;
    if (initial) {
        img = currentImage;
    }

    if (updated.length() && (initial || img.loadImage(updated)))
    {
        fileMove(updated, usedDir);
        currentImage = img;
        active = getActiveArea(img.getWidth(), img.getHeight());
        currentFrame = 0;
        ofLogNotice() << "got a new image '" << updated << "'" << img.getWidth() << "x" << img.getHeight() << endl;
        updated = "";
    }

    if (info) {
        info = false;
        fileMove(infoFile, usedDir);
    }

	if ((now - t0 > period * 4 / 5) &&
        (infoFile.length() != 0) && !info) {
        updated = infoFile;
        infoFile = "";
	}

    if (sndFile.length() != 0) {
        player.stop();
        player.loadSound(sndFile, false);
        sndFile = "";
        player.play();
    }

    ofBackground(255);
}

//--------------------------------------------------------------
void testApp::draw(){
    if (nImages > 0){
        // change no colour on the previous screen - draw it on white
        ofSetColor(ofColor::white);
        screen.draw(0, 0, scrWd, scrHt);
        if (alpha) {
            ofEnableAlphaBlending();
            ofSetColor(r, g, b, alpha);
        }

        currentImage.draw(active.x, active.y, active.width, active.height);
        if (alpha) {
            ofDisableAlphaBlending();
        }

        if ((currentFrame++ > 60) && !alpha) {
            reduceRect(active);
        }
        // slightly move the image
        if ((currentFrame % 10) && shake) {
            shakeRect(active);
        }
    } else {
        ofSetHexColor(0x999999);
        ofDrawBitmapString("no images found", 300, 80);
    }

    if (alpha) {
        screen.loadScreenData(0, 0, scrWd, scrHt);
    } else {
        screen.loadScreenData(spread, spread, scrWd - 2 * spread, scrHt - 2 * spread);
    }

    if (f1) {
        ofSetHexColor(0);
        ofRect(0, scrHt - 20, scrWd, 20);
        ofSetHexColor(0xffffff);
        char buf[100];
        sprintf(buf, "a %d sh %i p %i st %i RGB %d,%d,%d mx %i my %i outl %d sp %d",
                alpha, shake, period, stock, r, g, b, movX, movY, outline, spread);
        ofDrawBitmapString(buf, 5, scrHt - 5);
    }
}

//--------------------------------------------------------------
void testApp::keyPressed(int key) {
    switch (key)
    {
        case 'w':
        case 'W':
            ofToggleFullscreen();
            return;
        case 'p':
        case 'P':
            provider.capture();
            return;
        case 257: // F1
            f1 = !f1;
            break;
        case ' ':
            currentImage.loadImage("images\\deleted.jpg");
            currentFrame = 0;
            active = getActiveArea(currentImage.getWidth(), currentImage.getHeight());
            break;
        case 13:
        case 10:
            provider.revertTimer();
            break;
        default:
            break;
    }

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}

//--------------------------------------------------------------
ofRectangle testApp::getActiveArea(int width, int height)
{
    ofRectangle rect;

    scrWd = ofGetWidth();
    scrHt = ofGetHeight();

    float scX = scrWd / width;
    float scY = scrHt / height;
    float sc = scX < scY ? scX : scY;
    float wd2 = width * sc;
    float ht2 = height * sc;
    if (alpha) {
        rect.x = (scrWd - wd2) / 2;
        rect.y = (scrHt - ht2) / 2;
        rect.height = ht2;
        rect.width = wd2;
    } else {
        const float RND = 30.0;
        wd2 /= 2;
        ht2 /= 2;
        float cx = (scrWd - RND)/ 2 + ofRandom(0, RND);
        float cy = (scrHt - RND) / 2 + ofRandom(0, RND);
        rect.x = cx - ht2 / 2;
        rect.y = cy - wd2 / 2;
        rect.height = ht2;
        rect.width = wd2;
    }

    return rect;
}

//--------------------------------------------------------------
void testApp::shakeRect(ofRectangle &rct)
{
    int rnd = ofRandom(-shake, shake);
    rct.x += rnd + movX;
    rnd = ofRandom(-shake, shake);
    rct.y += rnd + movY;
}

//--------------------------------------------------------------
void testApp::reduceRect(ofRectangle &rct)
{
    rct.x++;
    rct.y++;
    rct.width -= 2;
    rct.height -= 2;
}


//--------------------------------------------------------------
void testApp::newMidiMessage(ofxMidiMessage& ev)
{
    if (!alphaCtr.handle(ev) && !shakeCtr.handle(ev) &&
        !lowCtr.handle(ev) && !highCtr.handle(ev) &&
        !rCtr.handle(ev) && !gCtr.handle(ev) && !bCtr.handle(ev) &&
        !periodCtr.handle(ev) && !stockCtr.handle(ev) &&
        !movXCtr.handle(ev) && !movYCtr.handle(ev) &&
        !spreadCtr.handle(ev) && !outlineCtr.handle(ev)) {
            cerr << "MIDI ch=" << ev.channel
                << " port=" << ev.portNum
                << " b1=" << ev.control
                << " b2=" << ev.value
                << " status=" << ev.status
                << endl;
        }
}

//--------------------------------------------------------------
bool MidiControl::init(ofxIniSettings &ini, string id) {
    mn = ini.get(id + "Mn", 0);
    mx = ini.get(id + "Mx", 127);
    ch = ini.get(id + "Ch", -1);
    nm = id;
    v = 0;
    fv = 0;
    return ch != -1;
}

//--------------------------------------------------------------
void MidiControl::init(ofxIniSettings &ini, string id, int *var) {
    if (init(ini, id)) v = var;
}

//--------------------------------------------------------------
void MidiControl::init(ofxIniSettings &ini, string id, float *var) {
    if (init(ini, id)) fv = var;
}

//--------------------------------------------------------------
bool MidiControl::handle(ofxMidiMessage& e) {
    if (e.control != ch) return false;
    int val = e.value;
    float res = float(val) * (mx - mn) / 127 + mn;
    cout << nm << " " << res << endl;
    if (v) *v = res;
    else if (fv) *fv = res;
    else return false;

    return true;
}

//--------------------------------------------------------------
bool testApp::getNewImages()
{
    string fn = provider.nextImageFile();
    if (fn.length() == 0) return false;
    int p = fn.find("-a.jpg");
    if (p != string::npos) {
        updated = fn;
        newimages(updated, fn.replace(p, 6, "-b.jpg"));
        return true;
    } else {
        updated = fn;
        cout << "not expected file name "<< fn << endl;
    }
    return false;
}

//--------------------------------------------------------------
void testApp::fileMove(string f, string dd)
{
    // only move from res dir
    if (f.find(resDir) == string::npos) return;
    ofFile of(f);
    string dst = dd + "\\" + of.getFileName();
    of.close();
    bool ok = ofFile::moveFromTo(f, dst, true, false);
    cout << "move: " << f << " to " << dst << " ok=" << ok << endl;
}

//--------------------------------------------------------------
CameraWriter::CameraWriter()
{
    string ts = ofGetTimestampString("%m%d%H%M");
    strcpy(prefix, ts.c_str());
}

//--------------------------------------------------------------
bool CameraWriter::init(string ids, ImageProvider *ip)
{
    m_ip = ip;
    char buf[1000];
    strcpy(buf, ids.c_str());
    for (char *p = strtok(buf, ", "); p; p = strtok(0, ", "))
    {
        int id = atoi(p);
        if(!initGrabber(id)) return false;
    }

    grIter = grabbers.begin();
    return grabbers.size() > 0;
}

//--------------------------------------------------------------
bool CameraWriter::initGrabber(int icam)
{
    ofVideoGrabber *gr = new ofVideoGrabber();
    gr->setDeviceID(icam);
    if (!gr->initGrabber(wd, ht)) {
        return false;
    }

    ofSleepMillis(camDelay / 2);

    while(true){
        gr->update();
        if (gr->isFrameNew()) break;
        ofSleepMillis(camDelay / 3);
    }

    ofSleepMillis(camDelay / 2);
    grabbers.insert(grabbers.begin(), gr);
    return true;
}

//--------------------------------------------------------------
void CameraWriter::capture()
{
    if (isThreadRunning()) { return; }
    startThread();
}

//--------------------------------------------------------------
void CameraWriter::setPath(string &p) {
    path = p;
}

//--------------------------------------------------------------
void CameraWriter::threadedFunction()
{
    if (serial) {
        // every write turns on a LED for a duration
        // determined in Arduino program
        unsigned char b = icam;
        serial->writeBytes(&b, 1);
        // cout << "to Arduino: " << b << endl;
    }

    ofVideoGrabber *cam = *grIter;
    icam++;
    if (++grIter == grabbers.end())
    {
        grIter = grabbers.begin();
        icam = 0;
    }

    cam->update();

	ofImage img;
    img.setFromPixels(cam->getPixels(), cam->width, cam->height, OF_IMAGE_COLOR, true);
    char buf[120];
    sprintf(buf, "%s-%05i-a", prefix, cnt);
    string f = path + "\\" + buf + ".jpg";
    cout << "saving " << f << endl;
    img.saveImage(f);
    cout << "saved " << f << endl;
    m_ip->addImageFile(f);

    imgPlayer::prepareBW(img, img, *outline);
    sprintf(buf, "%s-%05i-b", prefix, cnt++);
    f = path + "\\" + buf + ".jpg";
    img.saveImage(f);
    cout << "saved " << f << endl;
    cnt++;
    m_ip->revertTimer();
}

//--------------------------------------------------------------
void Pop3Writer::fetch()
{
    if (isThreadRunning()) { return; }
    startThread();
}

//--------------------------------------------------------------
void Pop3Writer::threadedFunction()
{
    int cnt = m_pop3.gotImages();
    if (cnt == 0) {
       cout << "no images in a mailbox, let's load local files" << endl;
   } else {
       cout << "email: " << cnt << " new images" << endl;
   }
   m_ip->revertTimer();
}

//--------------------------------------------------------------
void testApp::newimages(string a, string b)
{
    updated = a;
    imp.startConversion(b.c_str(), this, lowFreq, hiFreq);
}

//--------------------------------------------------------------
void ImageProvider::init(bool e, int p, int *s, string dir, string udir)
{
    t0 = ofGetElapsedTimeMillis();
    email = e;
    period = p;
    stock = s;
    stockDir = dir;
    usedDir = udir;
}

//--------------------------------------------------------------
void ImageProvider::addImageFile(string f)
{
    files.push_back(f);
}

//--------------------------------------------------------------
void ImageProvider::capture()
{
    t0 = ofGetElapsedTimeMillis() + EMAIL_TIMEOUT;
    state = Camera;
    cameraWriter.capture();
}

//--------------------------------------------------------------
string ImageProvider::nextImageFile()
{
    long now = ofGetElapsedTimeMillis();
    // don't do anything too soon
	if (now - t0 < 500) return "";

	if (now - t0 > period) {
	    switch (state)
	    {
	        case Init:
                if (email)
                {
                    // the t0 shall be reverted to current time when download ends
                    t0 = now + EMAIL_TIMEOUT;
                    // if there are no images in a mailbox, the pop3writer sets loadfile flag
                    pop3Writer.fetch();
                    state = Email;
                }
                else
                {
                    state = File;
                    break;
                }
                // fall through
	        case Email:
	        case Camera:
                return ""; // nothing yet
	        break;
	    }

        if (files.size() > 0)
        {
            t0 = now;
            string f = files.front();
            files.pop_front();
            state = Init;
            return f;
        }
        else
        {
            bool st = (rand() % 100) < (*stock);
            string d = st ? stockDir : usedDir;
            ofDirectory dir;
            int cnt = dir.listDir(d);
            if (!cnt) return "";
            int imgIdx = rand() % cnt;
            for(int i=0; i<cnt; ++i){
                string fname = dir.getPath(imgIdx % cnt);
                // OK if this is not the image made for sound generation
                if (fname.find("-b.jpg") == string::npos) {
                    cout << "image: " << fname << " found in " << d << " " << imgIdx << " " << cnt << endl;
                    t0 = now;
                    state = Init;
                    return fname;
                }
                imgIdx++;
            }
        }
 	}
    return "";
}

void Pop3Writer::setApp(ImageProvider *ip, testApp *a, ofxIniSettings &ini, string path, int *o) {
    m_ip = ip;
    m_pop3.init(ini);
    m_pop3.setPath(path, o, &a->provider);
}
