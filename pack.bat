@echo off
set DEST=..\..\..\..\perfStream
if exist %DEST%\nul goto already
mkdir %DEST%
mkdir %DEST%\data
mkdir %DEST%\data\images
mkdir %DEST%\data\images\res
mkdir %DEST%\data\images\stock
mkdir %DEST%\data\images\used
:already
copy bin\*.dll %DEST%
copy bin\*.exe %DEST%
copy bin\config.ini %DEST%\config.ini
copy bin\go.bat %DEST%
copy bin\data\images\deleted.jpg %DEST%\data\images
copy bin\data\images\bikers.jpg %DEST%\data\images
copy bin\data\images\stock\*.jpg %DEST%\data\images\stock

exit
pushd
cd %DEST%

popd
